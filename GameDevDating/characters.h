#pragma once
#include <SFML\Graphics.hpp>
#include <fstream>

using namespace sf;
using namespace std;



#ifndef GLOBALS
#define GLOBALS
enum backgrounds {
	done,			//go back to this when it changes so it doesn't keep loading from file
	outside,
	inside,
	bedroom
};

enum characters {
	NONE,
	TODD,
	GABE,
	CAGE,
	EA
};



class character {
public:
	Sprite sprite;
	Texture Tex;

	string name;

	bool onScreen = false;

	int score;

	character(string myName, string filename) {
		name = myName;
		Tex.loadFromFile("gameFiles/images/" + filename + ".png");
		sprite.setTexture(Tex);
	}
};

extern character todd;
extern character gabe;
extern character cage;
extern character ea;

#endif