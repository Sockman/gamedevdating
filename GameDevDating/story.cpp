#include "main.h"
using namespace std;

void gabeIntro();
void EAIntro();
void cageIntro();

void BethesdaConference();
void EAConference();

String credits = "Images from wikimedia commons. Authors: IFERREIRO,";

void story() {
	bgs = outside;
	writeText("You walk up to the the large, metallic convention center. You're excited because after months of planning and anticipation, you'll finally get to attend this year's G3 (Giant Gamer Gathering).");
	waitForClick();
	bgs = inside;
	writeText("You enter the convention center and almost instantly get lost in a crowd of people rushing somewhere. You eventually find yourself somewhere around the booths.");
	waitForClick();
	writeText("Not too far away someone catches your eye. He has messy hair, and is wearing a simple, plain brown t-shirt. It's a bit informal, but in a way, you find it charming.");
	waitForClick();
	writeText("He's signing autographs for a few people, so you debate whether you should go over to him. But before you can decide...");
	waitForClick();
	todd.onScreen = true;
	writeText("Hi, I'm Todd Howard. Big place huh? Easy to get lost in, so much to explore.", TODD);
	waitForClick();
	writeText("He looks up for a bit, and it seems like he's thinking about something nice.");
	waitForClick();
	writeText("Anyway, do you need anything? An autograph, a signed copy of Skyrim, a signed copy of Fallout 4, anything really.", TODD);
	switch (waitForButton("Can I have an autograph?", "Uhhhhh...", "Nah, I've never really been a fan of Skyrim.")) {
	case 0:
		todd.score += 1;
		writeText("Of course.", TODD);
		waitForClick();
		writeText("You fumble around for an old receipt.");
		waitForClick();
		writeText("Hey, don't bother with that. Here, I have this copy of Skyrim on me, I'll just sign it and let you have it.", TODD);
		waitForClick();
		writeText("He hands you a brand new copy of Skyrim for PC, with his signature scrawled on the back.");
		waitForClick();
		writeText("Why don't I give you a look around? I need to get back to my booth anyway.", TODD);
		break;
	case 1:
		writeText("A bit nervous? You know what, I'll show you around. This place can be intimidating at first, but trust me, everyone here is really nice.", TODD);
		break;
	case 2:
		todd.score -= 1;
		writeText("Oh uh... well that's new...", TODD);
		waitForClick();
		writeText("...", TODD);
		waitForClick();
		writeText("Uh... I gotta get back to my booth. So if you want a look around, I can show you some stuff on the way back.", TODD);
		break;
	}
	waitForClick();
	writeText("You follow Todd through the large convention center. He often turns around and changes directions, as if he were purposefully going off track.");
	waitForClick();
	if (todd.score < 0) {
		writeText("Perhaps he's trying to throw you off, but he remains silent until he gets to his booth.");
		waitForClick();
		writeText("Well, it was nice knowing you, I'll uh... see you around, I guess...", TODD);
	}
	else {
		writeText("Sorry for going off-track. I promise I know the way, I just really like adventure. This place is so huge, I want to explore every nook and cranny, you never know what you might find.", TODD);
		waitForClick();
		writeText("Anyway, it was nice meeting you.", TODD);
		waitForClick();
		if (todd.score > 0) {
			writeText("Maybe we should get together sometime, have a nice chat. Who knows? It will be a nice little adventure.", TODD);
		}
		else {
			writeText("I'll see you around.", TODD);
		}
	}
	waitForClick();
	todd.onScreen = false;
	writeText("After your encounter with Todd, you begin to explore the booths. You see EA very clearly. But next to it, and much smaller, you see the logo of another company. Could it be?");
	waitForClick();
	writeText("You go closer. It is! For the first time in years, Valve is at G3!");
	if (waitForButton("Go to EA's booth", "Go to Valve's booth") == 0) {
		writeText("You decide to go to EA's booth first. Valve probably doesn't have any games to show off anyway.");
		EAIntro();
		writeText("After that interesting encounter, you walk over to Valve's booth.");
		cageIntro();
		gabeIntro();
	}
	else {
		writeText("You decide to go to Valve's booth first. You just have to know what they're doing at G3. Maybe they even have a new game?");
		gabeIntro();
		if (gabe.score < 1) {
			writeText("Someone else gets his attention, and he's quickly distracted. Without much else to do there, you decide to head over to EA's booth.");
		}
		else {
			writeText("Anyway, it was nice to meet you.", GABE);
			waitForClick();
			writeText("Without much else to do there, you head over to EA's booth.");
		}
		cageIntro();
		EAIntro();
	}
	waitForClick();
	writeText("After meeting everyone you want to see, you head over to grab lunch. You see a few other booths but there's nothing too interesting. At the end of the day, you head home.");
	waitForClick();
	bgs = bedroom;
	writeText("After waking up, you get ready for another big day.");
	waitForClick();
	if (cage.score < 0) {
		writeText("You look at the schedule. Valve, EA, and Bethesda all have presentations on the same day. You also see Quantic Dream, and realize that the creepy guy you met yesterday was David Cage.");

	}
	else {
		writeText("You look at the schedule. Valve, EA, and Bethesda all have presentations on the same day. You also see Quantic Dream, and realize that the strange person you met yesterday was David Cage.");
	}
	waitForClick();
	writeText("You only have enough time to go to two, otherwise your schedule will be too packed. The earliest two are EA and Bethesda.");
	if(!waitForButton("EA", "Bethesda")) {
		EAConference();
	}
	else {
		BethesdaConference();
	}
}

void gabeIntro() {
	waitForClick();
	writeText("As you walk to the booth, you realize how small it is compared to the others. And surely enough, you spot the man himself.");
	gabe.onScreen = true;
	switch (waitForButton("I'm your biggest fan.", "...", "When's Half-Life 3?")) {
	case 0:
		gabe.score += 1;
		writeText("Uh... Thanks.", GABE);
		waitForClick();
		writeText("I know I look a bit, uh, bad, so uh, sorry about that.", GABE);
		waitForClick();
		writeText("Anyway, while you're here you might want to check out our new VR game.", GABE);
		waitForClick();
		writeText("It, uh, has some shooting, but we've tried to innovate a lot, you know, with VR.", GABE);
		waitForClick();
		writeText("So you want to try it out?", GABE);
		waitForClick();
		break;
	case 1:
		writeText("Uhh... hi...", GABE);
		waitForClick();
		writeText("I'm Gabe Newell, I work at Valve.", GABE);
		waitForClick();
		writeText("You can go check out our new game, we, uh, have it over there somewhere. It's in VR.", GABE);
		break;
	case 2:
		gabe.score -= 1;
		writeText("Uh... hehe... well we're not ready to talk about that yet.", GABE);
		waitForClick();
		writeText("We're just here because of our new uh... VR game. It's a shooter, but, uhh, in VR, and you can use abilities sometimes.", GABE);
		waitForClick();
		writeText("So, uh, you can play it if you want.", GABE);
		break;
	}

	if (waitForButton("Yeah, I'd be glad to play it.", "No, thanks.") == 0) {
		writeText("You walk over to an area where several VR headsets are set up. Gabe leads you to one and you put it on. The game looks amazing, and you're instantly immersed in its world. The gameplay is the most fun you've had in years.");
		waitForClick();
		writeText("Pretty good, isn't it? We've worked on it for a while.", GABE);
		waitForClick();
	}
	else {
		gabe.score -= 1;
		writeText("Ok, that's fine.", GABE);
		waitForClick();
	}
	gabe.onScreen = false;

}

void EAIntro() {
	waitForClick();
	writeText("EA's booth is huge, taking up what seems like a fourth of the entire convention center.");
	waitForClick();
	writeText("You walk to it, surrounded by dozens of screens running different games, and large signs and posters everywhere. Suddenly, you notice something behind you.");
	waitForClick();
	ea.onScreen = true;
	writeText("Hey, welcome to EA, the best gaming company on the planet! Feel free to play any of the games we have here! It's only $10 per game!",EA);
	bool playEAGame = true;
	int playerPlayEAGame = 3;
	switch (waitForButton("Sure! I'd be glad to.", "Wait why are you just a big EA logo?", "Uhh, no thanks...")) {
	case 0:
		playerPlayEAGame = 1;
		break;
	case 1:
		writeText("Corporation are people too, ya know. We can live our own lives, make our own decisions. You have an issue with that?", EA);
		switch (waitForButton("No! Not at all!","Yeah, of course I do.")) {
		case 0:
			writeText("Good, I was worried for a second there.", EA);
			waitForClick();
			writeText("Anyway, that offer still stands, it's $10 if you want to play.", EA);
			break;
		case 1:
			ea.score -= 1;
			writeText("Hey, who do you think you are?", EA);
			waitForClick();
			writeText("I'm the BIGGEST video game publisher on the PLANET. If you have an issue, just try to not play our games. Trust me, we're everywhere.", EA);
			waitForClick();
			writeText("He quickly directs you away from the booth and leaves you in the crowd.");
			playEAGame = false;
			break;
		}
		if (playEAGame) {
			switch (waitForButton("OK, here's $10.", "No, thanks.")) {
			case 0:
				playerPlayEAGame = 1;
				break;
			case 1:
				playerPlayEAGame = 0;
				break;
			}
		}
		break;
	case 2:
		playerPlayEAGame = 0;
		break;
	default:
		break;
	}
	if (playerPlayEAGame == 1) {
		ea.score += 1;
		writeText("Great! You'll definitely have fun, my games are the best there are. Nothing else stacks up.", EA);
		waitForClick();
		writeText("And if they do, we can just buy them! Anyway, have fun.", EA);
		waitForClick();
		writeText("You play a game for a bit. It's a sequel to a game you forget the name of, where you play as a father trying to find their lost son. Eventually you get bored and leave.");
	}
	else if(playerPlayEAGame == 0) {
		ea.score -= 1;
		writeText("Really? Why'd you even come here then?", EA);
		waitForClick();
		writeText("Do you want to just look at the games? Why'd you want to do that?", EA);
		waitForClick();
		writeText("Ugh, fine. Do whatever you want, I don't care.", EA);
		waitForClick();
		writeText("He walks away and leaves you alone.");
	}

	waitForClick();
	ea.onScreen = false;
}


void cageIntro() {
	waitForClick();
	writeText("On your way, you notice someone behind you. You look around and see an older man, with a bit of a goatee.");
	waitForClick();
	cage.onScreen = true;
	writeText("Uh... oh, hello. I'm, uh, sorry to bother you.",CAGE);
	waitForClick();
	writeText("He has a slight french accent, and seems to be very flustered.");
	waitForClick();
	writeText("Yeah, I'll move along, uh, sorry again.", CAGE);

	bool outcome = true;
	switch (waitForButton("Yeah get out of here you creep.", "No, it's fine. ", "uhh...")) {
	case 0: 
		cage.score -= 1;
		outcome = false;
		break;
	case 2:
		outcome = false;
		break;
	case 1:
		cage.score += 1;
		outcome = true;
		waitForClick();
		break;
	}

	if (outcome) {
		cage.onScreen = false;
		return;
	}
	cage.onScreen = false;
	waitForClick();
	writeText("He quickly leaves and becomes lost in the crowd.");
	waitForClick();
	writeText("After the strange encounter, you continue on your way.");
}

void EAConference() {

}

void BethesdaConference() {

}

void ValveConference() {

}

void CageConference() {

}