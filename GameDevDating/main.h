#pragma once
#include "characters.h"
using namespace std;

extern backgrounds bgs;
void writeText(string text, characters speaker = NONE);
void waitForClick();
int waitForButton(string button1, string button2, string button3 = "");
