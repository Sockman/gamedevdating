#include <SFML\Graphics.hpp>
#include <fstream>
#include "wraptext.hpp"
#include <boost/thread.hpp>
#include <future>
#include "characters.h"
#include "story.h"
#include <iostream> //for debugging

#define WIDTH 1280
#define HEIGHT 720

#define textSpeed 25
#define textSpeedFast 5

#define buttonHeight 100
using namespace sf;
using namespace std;

//initialize variables

//For text
int textProg = 0;
string currentText;
Clock textTimer;
bool writing = false;
bool finishwriting = true;
Text mainText;
Font mainFont;
RectangleShape textRect;

//Renderwindow
RenderWindow window(VideoMode(WIDTH, HEIGHT), "Game");

//background
backgrounds bgs = done;
Sprite backgroundSprite;
Texture backgroundTexture;

//keep track of mouse clicks/button clicks
bool clicked = false;
int drawbuttons = 0;

class button {
public:
	RectangleShape rect;
	Text text;
	bool pressed = false;

	button() {
		rect.setSize(Vector2f((WIDTH/3)-30, buttonHeight));
		rect.setFillColor(Color(0, 0, 255));
		rect.setOutlineThickness(5);
		rect.setOutlineColor(Color(0, 100, 255));
		rect.setPosition(WIDTH/2,HEIGHT/2);
		text.setString("example");
	}
	void updateText() {
		text.setPosition(rect.getPosition());
	}
};
button option1;
button option2;
button option3;

void writeText(string text, characters speaker = NONE) {

	switch (speaker) {
	case NONE:
		mainText.setFillColor(Color(255, 255, 255));
		break;
	case TODD:
		mainText.setFillColor(Color(255, 0, 255));
		break;
	case GABE:
		mainText.setFillColor(Color(0, 255, 255));
		break;
	case CAGE:
		mainText.setFillColor(Color(255, 255, 255));
		break;
	case EA:
		mainText.setFillColor(Color(255, 0, 0));
		break;
	default:
		break;
	}

	currentText = text;
	textProg = 0;
	string newText(' ', int(text.length()));
	mainText.setString("");

	writing = true;
}

void clickWait() {
	//Event e;
	//while (!window.pollEvent(e) || e.type != Event::MouseButtonPressed)
	//{
	//}
	while (!clicked) {
	}
	clicked = false;
}

void drawButtons(string button1, string button2, string button3, promise<int> * returnVal) {
	if (button3 == "") {
		drawbuttons = 2;
	}
	else {
		drawbuttons = 3;
	}
	option1.text.setString(wrapText(button1,option1.rect.getGlobalBounds().width,mainFont,30));
	option2.text.setString(wrapText(button2, option1.rect.getGlobalBounds().width, mainFont, 30));
	option3.text.setString(wrapText(button3, option1.rect.getGlobalBounds().width, mainFont, 30));
	if (button3 == "") {
		while (!option1.pressed && !option2.pressed) {

		}
	}
	else {
		while (!option1.pressed && !option2.pressed && !option3.pressed) {

		}
	}
	if (option1.pressed) {
		option1.pressed = false;
		returnVal->set_value(0);
	}
	else if(option2.pressed) {
		option2.pressed = false;
		returnVal->set_value(1);
	}
	else if (option3.pressed) {
		option3.pressed = false;
		returnVal->set_value(2);
	}
}

int waitForButton(string button1, string button2, string button3 = ""){
	promise<int> result;
	future<int> futureResult = result.get_future();
	boost::thread make(drawButtons, button1, button2, button3, &result);
	make.join();
	make.detach();
	drawbuttons = 0;
	return futureResult.get();
}

void waitForClick() {
	boost::thread wait(clickWait);
	wait.join();
	wait.detach();
}


void initialize() {

	boost::thread main(story);

	int rectWidth = WIDTH - 50;
	int rectHeight = HEIGHT / 4;

	//Text box
	textRect.setSize(Vector2f(rectWidth, rectHeight));
	textRect.setPosition(Vector2f(-(rectWidth - WIDTH)/2, HEIGHT - rectHeight-buttonHeight));
	textRect.setFillColor(Color(10, 10, 200, 200));
	textRect.setOutlineThickness(10);
	textRect.setOutlineColor(Color(10, 10, 150, 200));

	//fonts and text
	mainFont.loadFromFile("gameFiles/fonts/verdana.ttf");
	mainText.setFont(mainFont);
	mainText.setPosition(textRect.getPosition());
	mainText.setCharacterSize(30);

	//Character Sprites
	todd.sprite.setScale(HEIGHT/todd.sprite.getGlobalBounds().height, HEIGHT/todd.sprite.getGlobalBounds().height );
	todd.sprite.setPosition(WIDTH - todd.sprite.getGlobalBounds().width, 0);

	gabe.sprite.setScale(HEIGHT / gabe.sprite.getGlobalBounds().height, HEIGHT / gabe.sprite.getGlobalBounds().height);
	gabe.sprite.setPosition(0, 0);

	ea.sprite.setScale(HEIGHT / ea.sprite.getGlobalBounds().height, HEIGHT / ea.sprite.getGlobalBounds().height);
	ea.sprite.setPosition((WIDTH/2) - (ea.sprite.getGlobalBounds().width/2), 0);

	//buttons
	option1.text.setFont(mainFont);
	option2.text.setFont(mainFont);
	option3.text.setFont(mainFont);
	option1.rect.setPosition(10, HEIGHT - buttonHeight - 10);
	option2.rect.setPosition((WIDTH - (option1.rect.getSize().x)) - 10, HEIGHT - buttonHeight - 10);
	option3.rect.setPosition((WIDTH / 2) - (option1.rect.getSize().x) / 2, HEIGHT - buttonHeight - 10);
	option1.updateText();
	option2.updateText();
	option3.updateText();
}




void updateText() {
	if (finishwriting) {
		mainText.setString(wrapText(currentText, textRect.getGlobalBounds().width, mainFont, 30));
		finishwriting = false;
		writing = false;
	}
	else {
		if (textProg < currentText.length()) {
			if (textTimer.getElapsedTime().asMilliseconds() > textSpeed) {
				mainText.setString(mainText.getString() + currentText[textProg]);
				mainText.setString(wrapText(mainText.getString(), textRect.getGlobalBounds().width, mainFont, 30));
				textProg++;
				textTimer.restart();
			}
		}
		else {
			writing = false;
		}
	}
}

void setBackground(string bkgnd) {
	backgroundTexture.loadFromFile("gameFiles/images/" + bkgnd + ".jpg");
	backgroundSprite.setTexture(backgroundTexture);
	backgroundSprite.setScale(WIDTH / backgroundSprite.getGlobalBounds().width, WIDTH / backgroundSprite.getGlobalBounds().width);
	backgroundSprite.setTexture(backgroundTexture);
	bgs = done;
}

int main()
{
	//Initialize window, variables, ect.
	initialize();

	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed) {
				window.close();
			}
			else if (event.type == Event::MouseButtonPressed) {
				int mouseX = Mouse::getPosition(window).x;
				int mouseY = Mouse::getPosition(window).y;
				if (writing) {
					finishwriting = true;
				}
				switch (drawbuttons) {
				case 2:
					if (option1.rect.getGlobalBounds().contains(mouseX, mouseY)) {
						option1.pressed = true;
					}
					else if (option2.rect.getGlobalBounds().contains(mouseX, mouseY)) {
						option2.pressed = true;
					}
					break;
				case 3:
					if (option1.rect.getGlobalBounds().contains(mouseX, mouseY)) {
						option1.pressed = true;
					}
					else if (option2.rect.getGlobalBounds().contains(mouseX, mouseY)) {
						option2.pressed = true;
					}
					else if (option3.rect.getGlobalBounds().contains(mouseX, mouseY)) {
						option3.pressed = true;
					}
					break;
				default:
					if(!writing) {
						clicked = true;
					}
					break;
				}
			}
		}

		if (writing) {
			updateText();
		}

		window.clear();
		//Draw background (bottom layer, draw first)
		switch (bgs) {
		case outside:
			setBackground("outside");
			break;
		case inside:
			setBackground("inside");
			break;
		case bedroom:
			setBackground("bedroom");
			break;
		}
		window.draw(backgroundSprite);

		//Draw Characters
		if (todd.onScreen) {
			window.draw(todd.sprite);
		}
		if (gabe.onScreen) {
			window.draw(gabe.sprite);
		}
		if (cage.onScreen) {
			window.draw(cage.sprite);
		}
		if (ea.onScreen) {
			window.draw(ea.sprite);
		}

		//Draw HUD (Top Layer, draw last)
		window.draw(textRect);
		window.draw(mainText);

		switch (drawbuttons) {
		case 2:
			window.draw(option1.rect);
			window.draw(option2.rect);
			window.draw(option1.text);
			window.draw(option2.text);
			break;
		case 3:
			window.draw(option1.rect);
			window.draw(option2.rect);
			window.draw(option3.rect);
			window.draw(option1.text);
			window.draw(option2.text);
			window.draw(option3.text);
			break;
		}
		window.display();
	}
	return 0;
}
